Pod::Spec.new do |s|
    s.name             = 'OkayIDNFC'
    s.version          = '1.0.9'
    s.license          = { :file => 'LICENSE', :type => 'Commercial' }
    s.author           = { 'Innov8tif' => 'ekyc.team@innov8tif.com' }
    s.homepage         = 'https://innov8tif.com'
    s.summary          = 'OkayIDNFC iOS SDK.'
    s.source           = { :git => 'https://gitlab.com/innov8tif-ekyc-product/cocoapods/okaynfc-ios.git', :tag => 'v1.0.9', :branch => 'main' }
    s.platform = :ios
    s.user_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
    s.pod_target_xcconfig = { 
	'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64',
	'IPHONEOS_DEPLOYMENT_TARGET' => '13.0' 
	}
    s.ios.deployment_target = '13.0'
    s.ios.vendored_frameworks = 'OkayIDNFC.xcframework'
    s.swift_version = '5.3'
    s.resources = ['Assets/masterList.pem']
    s.dependency 'OpenSSL-Universal', '1.1.2200'
    s.dependency 'CryptoSwift', '~> 1.8.2'
  end

