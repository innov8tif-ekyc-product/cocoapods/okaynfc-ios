# OkayIDNFC

![Version](https://img.shields.io/cocoapods/v/OkayNFC.svg?style=flat)
![Platform](https://img.shields.io/cocoapods/p/OkayNFC.svg?style=flat)

Requires iOS 13+ and Swift 5.

## Contents

- [Installation](#installation)
- [Permission](#permission)
- [Usage](#usage)
  - [Scan NFC](#scan-nfc)
- [Required Inputs](#required-inputs)
  - [MRZ Key](#mrz-key)
  - [Data Group](#data-group)
  - [NFCViewDisplayMessage](#nfcviewdisplaymessage)
  - [readPassport](#readpassport)
- [Result](#result-okaynfcresult)
- [Error Handling](#error-handling-nfcpassportreadererror)

## Installation

OkayIDNFC is available through [CocoaPods](https://cocoapods.org). To install
it, add the following to your app target in your podfile:

```ruby
use_frameworks!
pod 'OkayIDNFC'

post_install do |installer|
    installer.pods_project.targets.each do |target|
        if target.name == "CryptoSwift"
            puts "Enable module stability for CryptoSwift"
            target.build_configurations.each do |config|
                config.build_settings['BUILD_LIBRARY_FOR_DISTRIBUTION'] = 'YES'
            end
        end
    end
end
```

## Permission

1. Add `NFC` capability to the project

   <img src="images/image1.png" width="700">
   <img src="images/image2.png" width="700">

2. In `info.plist`, add the keys as the image attached below.

   <img src="images/image3.png" width="700">

## Usage

Start by importing `OkayIDNFC` module into your swift file.

```swift
import OkayIDNFC
```

### Scan NFC

1. Instantiate the `PassportReader` class

```swift
private let passportReader = PassportReader()
```

2. Retrieve the MRZ key by passing in `passport number`, `birthdate` and `expiryDate`

```swift
 let mrzKey = PassportUtils().getMRZKey(passportNumber: passportNumber, dateOfBirth: birthDateString, dateOfExpiry: expiryDateString)
```

3. License key is needed to trigger the process. Start the process by calling `readPassport` on `passportReader`. You may handle the result in the completion handler.

```swift
passportReader.readPassport(license: licenseKey, mrzKey: mrzKey,
     completed: { (passport, error) in
        if let passport = passport {
            // handle passport result
        }
        else {
            // handle error
        }
    })
```

## Required Inputs

### MRZ Key

| Parameter      | Type   | Default | Required format |
| -------------- | ------ | ------- | --------------- |
| passportNumber | String | nil     |                 |
| dateOfBirth    | String | nil     | yyMMdd          |
| dateOfExpiry   | String | nil     | yyMMdd          |

### Data Group

| Parameter                                | Description |
| ---------------------------------------- | ----------- |
| DG1                                      | MRZ Data    |
| DG2                                      | Image       |
| DG7, DG11, DG12, DG14, DG15, COM and SOD | -           |

### NFCViewDisplayMessage

```swift
case .requestPresentPassport:
    return "Hold your iPhone near an NFC enabled passport."
case .authenticatingWithPassport(let progress):
    let progressString = handleProgress(percentualProgress: progress)
    return "Authenticating with passport.....\n\n\(progressString)"
case .readingDataGroupProgress(let dataGroup, let progress):
    let progressString = handleProgress(percentualProgress: progress)
    return "Reading \(dataGroup).....\n\n\(progressString)"
case .error(let tagError):
    switch tagError {
        case NFCPassportReaderError.PassportNumberEmpty:
            return "Passport number cannot be empty"
        case NFCPassportReaderError.BirthDateEmpty:
            return "Please select your birth date"
        case NFCPassportReaderError.ExpiryDateEmpty:
            return "Please select your passport expiry date"        
        case NFCPassportReaderError.TagNotValid:
            return "Tag not valid."
        case NFCPassportReaderError.MoreThanOneTagFound:
            return "More than 1 tags was found. Please present only 1 tag."
        case NFCPassportReaderError.ConnectionError:
            return "Connection error. Please try again."
        case NFCPassportReaderError.InvalidMRZKey:
            return "MRZ Key not valid for this document."
        case NFCPassportReaderError.ResponseError(let description, let sw1, let sw2):
            return "Sorry, there was a problem reading the passport. \(description) - (0x\(sw1), 0x\(sw2)"
        default:
            return "Sorry, there was a problem reading the passport. Please try again"
    }
case .successfulRead:
    return "Passport read successfully"
```

### readPassport

| Parameter            | Type                               | Default | Required   |
| -------------------- | ---------------------------------- | ------- | ---------- |
| licenseKey           | String                             | nil     | compulsory |
| mrzKey               | String                             | nil     | compulsory |
| tags                 | [DataGroupId]                      | []      | optional   |
| skipSecureElements   | Bool                               | true    | optional   |
| skipCA               | Bool                               | false   | optional   |
| skipPACE             | Bool                               | false   | optional   |
| customDisplayMessage | (NFCViewDisplayMessage) -> String? | nil     | optional   |

## Result (OkayNFCResult)

| Parameter          | Type   | Default |
| ------------------ | ------ | ------- |
| documentType       | String | ""      |
| documentNumber     | String | ""      |
| personalNumber     | String | ""      |
| name               | String | ""      |
| gender             | String | ""      |
| nationality        | String | ""      |
| dateOfBirth        | String | ""      |
| documentExpiryDate | String | ""      |
| faceImgPath        | URL    | nil     |

## Error Handling (NFCPassportReaderError)

The error received in the completed handler

```swift
case ResponseError(String, UInt8, UInt8)
case InvalidResponse
case UnexpectedError
case NFCNotSupported
case NoConnectedTag
case D087Malformed
case InvalidResponseChecksum
case MissingMandatoryFields
case CannotDecodeASN1Length
case InvalidASN1Value
case UnableToProtectAPDU
case UnableToUnprotectAPDU
case UnsupportedDataGroup
case DataGroupNotRead
case UnknownTag
case UnknownImageFormat
case NotImplemented
case TagNotValid
case ConnectionError
case UserCanceled
case InvalidMRZKey
case MoreThanOneTagFound
case InvalidHashAlgorithmSpecified
case InvalidDataPassed(String)
case NotYetSupported(String)
case InvalidLicenseKey
case PermissionDenied
case Error(String)
case PassportNumberEmpty
case BirthDateEmpty
case ExpiryDateEmpty
```
